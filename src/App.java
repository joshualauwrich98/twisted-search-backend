import com.google.gson.*;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import util.ApplicationUtil;
import util.Logger;
import util.Parser;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.URI;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Main class
 * Act as the main API server
 *
 * @author TwiseAdmin
 */
public class App {

    private static volatile AtomicBoolean settings = new AtomicBoolean(false);
    private static volatile AtomicBoolean index = new AtomicBoolean(false);

    public static void main(String[] args) throws IOException {
        HttpServer server = HttpServer.create(new InetSocketAddress(ApplicationUtil.PORT), 0);
        server.createContext("/", new MyServer());
        server.start();
        Parser.getInstance();
        Logger.getInstance().info("Server started:");
        Logger.getInstance().info("Server run on port 8998... Ready to serve RESTFUL API.... ");
        try {
            ApplicationUtil.getInstance().updateCosineNormalization();
        } catch (IOException e) {
            Logger.getInstance().error("Normalization file not found");
        }
        try {
            ApplicationUtil.getInstance().updateDocumentLength();
        } catch (IOException e) {
            Logger.getInstance().error("Document summary file not found");
        }
    }

    private static class MyServer implements HttpHandler {

        @Override
        public void handle(HttpExchange he) throws IOException {
            Logger.getInstance().info("Receiving request: " + he.getRequestMethod());

            Gson gson = new GsonBuilder().serializeNulls().create();
            String response = "";
            URI requestURI = he.getRequestURI();
            Headers responHeaders = he.getResponseHeaders();
            responHeaders.add("Content-type", "application/json");
            OutputStream os = he.getResponseBody();
            int length;

            if (he.getRequestMethod().equalsIgnoreCase("GET")) {
                Logger.getInstance().info("Request to " + requestURI.getPath());

                switch (requestURI.getPath().substring(1).toLowerCase()) {
                    case "search":
                        String[] query = requestURI.getQuery().split("&");
                        HashMap<String, String> queryMap = convertQueryToMap(query);
                        String querySentence = queryMap.get("searchQuery").replaceAll("\\+", " ");
                        response = Parser.getInstance().doRankedQuery(querySentence);
                        if (response == null || response.isEmpty()) response = "{null}";
                        Logger.getInstance().info("Sending response: " + (response.length() > 1000 ? response.substring(0,1000) : response));
                        length = response.getBytes().length;
                        responHeaders.add("Access-Control-Allow-Origin", "*");
                        he.sendResponseHeaders(HttpURLConnection.HTTP_OK, length);
                        break;
                    case "config":
                        response = gson.toJson(settings.get());
                        Logger.getInstance().info("Sending response: " + (response.length() > 1000 ? response.substring(0,1000) : response));
                        length = response.getBytes().length;
                        responHeaders.add("Access-Control-Allow-Origin", "*");
                        he.sendResponseHeaders(HttpURLConnection.HTTP_OK, length);
                        break;
                    case "check_index":
                        response = Boolean.toString(ApplicationUtil.getInstance().getTotalNumberOfFiles(ApplicationUtil.getInstance().getDataSortedBlock()) > 0);
                        Logger.getInstance().info("Sending response: " + (response.length() > 1000 ? response.substring(0,1000) : response));
                        length = response.getBytes().length;
                        responHeaders.add("Access-Control-Allow-Origin", "*");
                        he.sendResponseHeaders(HttpURLConnection.HTTP_OK, length);
                        break;
                    case "create_index":
                        Parser.getInstance().automateIndexing();
                        ApplicationUtil.getInstance().updateCosineNormalization();
                        ApplicationUtil.getInstance().updateDocumentLength();
                        index.compareAndSet(false,true);
                        response = "true";
                        Logger.getInstance().info("Sending response: " + response);
                        length = response.getBytes().length;
                        responHeaders.add("Access-Control-Allow-Origin", "*");
                        he.sendResponseHeaders(HttpURLConnection.HTTP_OK, length);
                        break;
                    case "index" :
                        if (ApplicationUtil.getInstance().getTotalNumberOfFiles(ApplicationUtil.getInstance().getDataSortedBlock()) > 0) {
                            response = Parser.getInstance().getAllIndex();
                        } else {
                            response = "{null}";
                        }
                        Logger.getInstance().info("Sending response: " + (response.length() > 1000 ? response.substring(0,1000) : response));
                        length = response.getBytes().length;
                        responHeaders.add("Access-Control-Allow-Origin", "*");
                        he.sendResponseHeaders(HttpURLConnection.HTTP_OK, length);
                        break;
                    default:
                        response = Integer.toString(Logger.getInstance().error("Found 404 Error"));
                        responHeaders.add("Access-Control-Allow-Origin", "*");
                        length = response.getBytes().length;
                        he.sendResponseHeaders(HttpURLConnection.HTTP_NOT_FOUND, length);
                }
            } else if (he.getRequestMethod().equalsIgnoreCase("POST")) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(he.getRequestBody()));
                StringBuilder requestBody = new StringBuilder();
                String temp;
                while ((temp = reader.readLine()) != null) {
                    requestBody.append(temp);
                }
                JsonElement parser = JsonParser.parseString(requestBody.toString());
                JsonObject requestObject = parser.getAsJsonObject();

                Logger.getInstance().info("Request to " + requestURI.getPath());

                switch (requestURI.getPath().substring(1).toLowerCase()) {
                    case "config":
                        String path = requestObject.get("path") instanceof JsonNull ? null : requestObject.get("path").getAsString();
                        boolean isNer = requestObject.get("ner").getAsBoolean();
                        boolean isDictionaryCompressed = requestObject.get("dictionary").getAsBoolean();
                        boolean isPostingCompressed = requestObject.get("posting").getAsBoolean();
                        boolean verbose = requestObject.get("verbose").getAsBoolean();
                        long maxMemory = Long.parseLong(requestObject.get("memory").getAsString()) * 1024 * 1024;
                        int topRank = Integer.parseInt(requestObject.get("top_rank").getAsString());

                        ApplicationUtil.getInstance().init(path, isNer, isPostingCompressed, isDictionaryCompressed, verbose, maxMemory, topRank);
                        settings.compareAndSet(false, true);
                        Parser.getInstance().updateDocumentFilename();

                        Logger.getInstance().info("Receive request body: " + requestObject.toString());

                        response = gson.toJson(settings.get());
                        Logger.getInstance().info("Sending response: " + response);
                        length = response.getBytes().length;
                        responHeaders.add("Access-Control-Allow-Origin", "*");
                        he.sendResponseHeaders(HttpURLConnection.HTTP_OK, length);
                        break;
                    case "login":
                        String username = requestObject.get("username").getAsString();
                        String password = requestObject.get("password").getAsString();
                        Logger.getInstance().info("Receiving login request as " + username + " with password " + password);

                         {
                            try {
                                if (ApplicationUtil.USERNAME.equals(username) && ApplicationUtil.ADMIN_PASS.equals(ApplicationUtil.getInstance().encrypt(password))) {
                                    response = "true";
                                } else {
                                    response = "false";
                                }
                                responHeaders.add("Access-Control-Allow-Origin", "*");
                                length = response.getBytes().length;
                                he.sendResponseHeaders(HttpURLConnection.HTTP_OK, length);
                            } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException | UnsupportedEncodingException | InvalidKeySpecException ex) {
                                response = Integer.toString(Logger.getInstance().error("Internal server error!"));
                                responHeaders.add("Access-Control-Allow-Origin", "*");
                                length = response.getBytes().length;
                                he.sendResponseHeaders(HttpURLConnection.HTTP_INTERNAL_ERROR, length);
                            }
                        }
                        break;
                    default:
                        response = Integer.toString(Logger.getInstance().error("Found 404 Error"));
                        responHeaders.add("Access-Control-Allow-Origin", "*");
                        length = response.getBytes().length;
                        he.sendResponseHeaders(HttpURLConnection.HTTP_NOT_FOUND, length);
                }
            } else if (he.getRequestMethod().equalsIgnoreCase("OPTIONS")) {
                he.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
                he.getResponseHeaders().add("Access-Control-Allow-Methods", "GET, OPTIONS");
                he.getResponseHeaders().add("Access-Control-Allow-Headers", "Content-Type,Authorization");
                he.sendResponseHeaders(HttpURLConnection.HTTP_NO_CONTENT, -1);
            } else {
                response = Integer.toString(Logger.getInstance().error("Found 405 Error"));
                responHeaders.add("Access-Control-Allow-Origin", "*");
                length = response.getBytes().length;
                he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, length);
            }
            os.write(response.getBytes());
            os.close();
            he.close();
            Logger.getInstance().info("Successfully sent response body!");
        }

        private HashMap<String, String> convertQueryToMap(String... queries) {
            HashMap<String, String> result = new HashMap<>();
            String[] temp;
            for (String query : queries) {
                temp = query.split("=");
                result.put(temp[0], temp[1]);
            }
            return result;
        }
    }
}
