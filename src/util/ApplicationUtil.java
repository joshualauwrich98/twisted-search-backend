package util;

import com.google.gson.Gson;

import java.io.*;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.*;
import javax.crypto.BadPaddingException;
import javax.crypto.KeyGenerator;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import com.google.gson.reflect.TypeToken;
import model.Document;
import model.Posting;
import model.Term;
import tokenize.Lemmatizer;

/**
 * Class for application's utility
 *
 * @author TwiseAdmin
 */
public class ApplicationUtil {

    public static final int PORT = 8998;                  //server port
    public static final char[] PASSWORD = "pYYMA1ct50OTcFjpnlXd".toCharArray();
    public static final byte[] SALT = "TZegwqqt6yE4hFSXKg3n".getBytes();
    public static final String USERNAME = "admin";
    public static final String ADMIN_PASS = "659299912";
    public static final double BM25_K1 = 1.2;
    public static final double BM25_K3 = 2.0;
    public static final double BM25_B = 0.75;
    public static final double LAMDA = 0.125;

    //App Settings
    private boolean verbose;                        //enable logging
    private boolean NER;                            //enable named entity recognition
    private boolean postingCompression;             //enable posting compression
    private boolean dictionaryCompression;          //enable dictionary compression
    private int topRank;                            //how much top result will be shown to user
    private String root;                            //root file path
    private String documentSourcePath;              //path for indexed folder
    private long maxMemoryIndexing;                 //maximum memory used for indexing in byte(s)
    private double [] cosineNormalizationData;      //cosine normalization
    private int [] documentLength;                  //length of each document

    private static class ApplicationUtilCreator {

        private static final ApplicationUtil INSTANCE = new ApplicationUtil();
    }

    public static ApplicationUtil getInstance() {
        return ApplicationUtilCreator.INSTANCE;
    }

    private ApplicationUtil() {
        this.verbose = true;
        this.NER = false;
        this.postingCompression = false;
        this.dictionaryCompression = false;
        this.topRank = 0;
        this.root = System.getProperty("user.dir") + File.separator;
        this.documentSourcePath = null;
        this.createRequiredDirectory();
        this.maxMemoryIndexing = 500000000;
    }

    public boolean isVerbose() {
        return verbose;
    }

    public boolean isNer() {
        return NER;
    }

    public boolean isPostingCompression() {
        return postingCompression;
    }

    public boolean isDictionaryCompression() {
        return dictionaryCompression;
    }

    public String getRoot() {
        return root;
    }

    public String getDataPath() {
        return this.root + "data" + File.separator + "src";
    }

    public String getDataTempBlock() {
        return this.root + "data" + File.separator + "store" + File.separator + "temp_block";
    }

    public String getDataSortedBlock() {
        return this.root + "data" + File.separator + "store" + File.separator + "sorted_block";
    }

    public String getCosineNormalizationPath() {
        return this.root + "data" + File.separator + "store" + File.separator + "cosine_normalization";
    }

    public String getDocumentData() {
        return this.root + "data" + File.separator + "store" + File.separator + "docs";
    }

    public double [] getCosineNormalizationData() {
        return this.cosineNormalizationData;
    }

    public void updateCosineNormalization() throws IOException {
        this.cosineNormalizationData = readCosineNormalizationData();
    }

    public int [] getDocumentLength() { return this.documentLength; }

    public void updateDocumentLength() throws IOException {
        this.documentLength = readDocumentLength();
    }

    public long getMaxMemoryIndexing() {
        return maxMemoryIndexing;
    }

    public String getDocumentSourcePath() {
        return documentSourcePath;
    }

    public void init(String path, boolean NER, boolean postingCompression, boolean dictionaryCompression, boolean verbose, long maxMemory, int topRank) {
        if (path != null) {
            if (path.charAt(path.length() - 1) == '/' || path.charAt(path.length() - 1) == '\\') {
                this.documentSourcePath = path.substring(0, path.length() - 1) + File.separator;
            } else {
                this.documentSourcePath = path + File.separator;
            }
        }

        this.topRank = topRank;
        if (this.NER != NER) {
            this.NER = NER;
            if (this.NER) Lemmatizer.getInstance().makeNERAvailable();
            else Lemmatizer.getInstance().makeNERUnavailable();
        }

        this.postingCompression = postingCompression;
        this.dictionaryCompression = dictionaryCompression;
        this.verbose = verbose;
        this.maxMemoryIndexing = maxMemory;
    }

    private void createRequiredDirectory() {
        File temp = new File(getDataPath());
        if (!temp.exists()) {
            temp.mkdirs();
        }
        temp = new File(getDataTempBlock());
        if (!temp.exists()) {
            temp.mkdirs();
        }
        temp = new File(getDataSortedBlock());
        if (!temp.exists()) {
            temp.mkdirs();
        }
        temp = new File(getDocumentData());
        if (!temp.exists()) {
            temp.mkdirs();
        }
        temp = new File(getCosineNormalizationPath());
        if (!temp.exists()) {
            temp.mkdirs();
        }
    }

    public int getTopRank() {
        return topRank;
    }

    /**
     * Print log for education purpose
     *
     * @throws IOException
     */
    public void printLog() throws IOException {
        Logger.getInstance().info("Total word from all docs: " + getTotalWordFromAllDocs());
        Logger.getInstance().info("Average word from all docs: " + getAvgWordFromAllDocs());
        Logger.getInstance().info("Total term awal from all docs: " + getTotalTermAwalFromAllDocs());
        Logger.getInstance().info("Average term awal from all docs: " + getAvgTermAwalFromAllDocs());
        Logger.getInstance().info("Total term after cleaning from all docs: " + getTotalTermACleaningFromAllDocs());
        Logger.getInstance().info("Average term after cleaning from all docs: " + getAvgTermACleaningFromAllDocs());
        Logger.getInstance().info("Total term after lemmatization from all docs: " + getTotalTermALemmatizationFromAllDocs());
        Logger.getInstance().info("Average term after lemmatization from all docs: " + getAvgTermALemmatizationFromAllDocs());
        Logger.getInstance().info("Total term after removing stop words and stemming from all docs: " + getTotalTermAStemmingFromAllDocs());
        Logger.getInstance().info("Average term after removing stop words and stemming from all docs: " + getAvgTermAStemmingFromAllDocs());

        this.printNERCategory();
    }

    private int getTotalWordFromAllDocs() throws FileNotFoundException, IOException {
        int res = 0;
        for (int i = 0; i < getTotalNumberOfFiles(getDocumentData()); i++) {
            File file = new File(getDocumentData(), i + ".twise");
            BufferedReader reader = new BufferedReader(new FileReader(file));
            Gson a = new Gson();
            res += a.fromJson(reader.readLine(), Document.class).getTotalWord();
        }
        return res;
    }

    private int getAvgWordFromAllDocs() throws IOException {
        return getTotalWordFromAllDocs() / getTotalNumberOfFiles(getDocumentData());
    }

    private int getTotalTermAwalFromAllDocs() throws FileNotFoundException, IOException {
        int res = 0;
        for (int i = 0; i < getTotalNumberOfFiles(getDocumentData()); i++) {
            File file = new File(getDocumentData(), i + ".twise");
            BufferedReader reader = new BufferedReader(new FileReader(file));
            Gson a = new Gson();
            res += a.fromJson(reader.readLine(), Document.class).getTotalTerm();
        }
        return res;
    }

    private int getAvgTermAwalFromAllDocs() throws IOException {
        return getTotalTermAwalFromAllDocs() / getTotalNumberOfFiles(getDocumentData());
    }

    private int getTotalTermACleaningFromAllDocs() throws FileNotFoundException, IOException {
        int res = 0;
        for (int i = 0; i < getTotalNumberOfFiles(getDocumentData()); i++) {
            File file = new File(getDocumentData(), i + ".twise");
            BufferedReader reader = new BufferedReader(new FileReader(file));
            Gson a = new Gson();
            res += a.fromJson(reader.readLine(), Document.class).getTotalTermAfterCleaning();
        }
        return res;
    }

    private int getAvgTermACleaningFromAllDocs() throws IOException {
        return getTotalTermACleaningFromAllDocs() / getTotalNumberOfFiles(getDocumentData());
    }

    private int getTotalTermALemmatizationFromAllDocs() throws FileNotFoundException, IOException {
        int res = 0;
        for (int i = 0; i < getTotalNumberOfFiles(getDocumentData()); i++) {
            File file = new File(getDocumentData(), i + ".twise");
            BufferedReader reader = new BufferedReader(new FileReader(file));
            Gson a = new Gson();
            res += a.fromJson(reader.readLine(), Document.class).getTotalTermAfterLemmatization();
        }
        return res;
    }

    private int getAvgTermALemmatizationFromAllDocs() throws IOException {
        return getTotalTermALemmatizationFromAllDocs() / getTotalNumberOfFiles(getDocumentData());
    }

    private int getTotalTermAStemmingFromAllDocs() throws FileNotFoundException, IOException {
        int res = 0;
        for (int i = 0; i < getTotalNumberOfFiles(getDocumentData()); i++) {
            File file = new File(getDocumentData(), i + ".twise");
            BufferedReader reader = new BufferedReader(new FileReader(file));
            Gson a = new Gson();
            res += a.fromJson(reader.readLine(), Document.class).getTotalTermAfterStemming();
        }
        return res;
    }

    private int getAvgTermAStemmingFromAllDocs() throws IOException {
        return getTotalTermAStemmingFromAllDocs() / getTotalNumberOfFiles(getDocumentData());
    }

    private void printNERCategory() throws IOException {
        int totalIp = 0, totalNumber = 0, totalObject = 0, totalPerson = 0, totalLocation = 0, totalDate = 0, totalTime = 0;
        Document x;
        for (int i = 0; i < getTotalNumberOfFiles(getDocumentData()); i++) {
            File file = new File(getDocumentData(), i + ".twise");
            BufferedReader reader = new BufferedReader(new FileReader(file));
            Gson a = new Gson();
            x = a.fromJson(reader.readLine(), Document.class);
            totalIp += x.getTotalIp();
            totalNumber += x.getTotalNumber();
            totalObject += x.getTotalObject();
            totalPerson += x.getTotalPerson();
            totalLocation += x.getTotalLocation();
            totalDate += x.getTotalDate();
            totalTime += x.getTotalTime();
        }

        Logger.getInstance().info("Total person: " + totalPerson);
        Logger.getInstance().info("Total number: " + totalNumber);
        Logger.getInstance().info("Total location: " + totalLocation);
        Logger.getInstance().info("Total date: " + totalDate);
        Logger.getInstance().info("Total time: " + totalTime);
        Logger.getInstance().info("Total number: " + totalNumber);
        Logger.getInstance().info("Total ip: " + totalIp);
        Logger.getInstance().info("Total object: " + totalObject);
    }

    /**
     * Count the files in a directory
     *
     * @param dir path to the directory that will be checked
     * @return the number of the files in the directory
     */
    public int getTotalNumberOfFiles(String dir) {
        return getAllFiles(dir).length;
    }

    /**
     * Get all the files in a directory
     *
     * @param dir path to the directory
     * @return all files in the directory specify by the parameter
     */
    public File[] getAllFiles(String dir) {
        return new File(dir).listFiles();
    }

    /**
     * Count all the words in a string
     *
     * @param input the input string
     * @return the number of the words in input string
     */
    public int countTotalWordsFromString(String input) {
        return getWordsFromString(input).length;
    }

    /**
     * Count all the term in a string
     *
     * @param input the input string
     * @return the number of the term in input string
     */
    public int countTotalTermFromString(String input) {
        Set<String> mySet = new HashSet<>();
        Collections.addAll(mySet, getWordsFromString(input));
        return mySet.size();
    }

    /**
     * Get all the words from a string
     *
     * @param input the input string
     * @return all the words in input string
     */
    public String[] getWordsFromString(String input) {
        List<String> list = new ArrayList<>();
        int pos = 0, end;
        while ((end = input.indexOf(' ', pos)) >= 0) {
            list.add(input.substring(pos, end).trim());
            pos = end + 1;
        }
        list.removeAll(Arrays.asList("", " ", "\n"));
        String[] words = new String[list.size()];
        words = list.toArray(words);

        return words;
    }

    /**
     * Write the document summary that can be used for pre-processing for the
     * search engine. The docs will be saved in the directory which is specified
     * with the (this.root + getDocumentData()) path
     *
     * @param document the document object
     * @throws IOException
     */
    public void writeDocumentSummary(Document document) throws IOException {
        File doc = new File(getDocumentData(), document.getId() + ".twise");
        FileWriter writer = new FileWriter(doc);

        writer.append(document.toString());
        writer.flush();
        writer.close();

        Logger.getInstance().info("Success write block: " + doc.length() * 1.0 / 1024 + "KB");
    }

    /**
     * Write the inverted block to the disk to clean up the memory
     *
     * @param data the inverted block to be saved to disk
     * @throws IOException
     */
    public void writeInvertedIndexBlock(TreeMap<String, TreeMap<Integer, Posting>> data) throws IOException {
        File temp = new File(getDataTempBlock(), (getTotalNumberOfFiles(getDataTempBlock()) + 1) + ".twise");

        Files.write(temp.toPath(), new Gson().toJson(data).getBytes());

        Logger.getInstance().info("Finished writing inverted index: " + temp.getAbsolutePath());
        Logger.getInstance().info("Size: " + temp.length() * 1.0 / 1024 + "KB");
        Logger.getInstance().info("Document temporary blocked store in " + this.root + getDocumentData());
    }

    public String[] getAllFilename(File [] files) {
        if (files == null) return null;
        String [] result = new String[files.length];
        int i = 0;
        for (File file: files) {
            result[i++] = file.getName();
        }
        return result;
    }

    public String[] getStopWords() throws IOException {
        ArrayList<String> data = new ArrayList<>();
        InputStream in = ApplicationUtil.class.getResourceAsStream("/lib/stop_words.txt");
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        String s;
        while ((s = reader.readLine()) != null) {
            data.add(s);
        }

        String[] result = new String[data.size()];
        result = data.toArray(result);

        return result;
    }

    public String encrypt(String text) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, InvalidKeySpecException {
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        KeySpec spec = new PBEKeySpec(PASSWORD, SALT, 65536, 256);
        SecretKey tmp = factory.generateSecret(spec);
        SecretKey secret = new SecretKeySpec(tmp.getEncoded(), "AES");

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, secret);

//        return new String(cipher.doFinal(text.getBytes("UTF-8")), "UTF-8");
        return Integer.toString(text.hashCode());
    }

    public double [] calculateBM25Scores(HashMap<String, Integer> queryData, Set<Integer> resultDocs) {
        int totalDoc = totalDocument();
        double[] scores = new double[totalDoc];
        double temp;
        try {
            if (documentLength == null) documentLength = readDocumentLength();
            TreeMap<Integer, Posting> postingList;
            int documentAvg = getAvgTermAStemmingFromAllDocs();
            for (String queryTerm: queryData.keySet()) {
                postingList = fetchPostingList(queryTerm);
                for (int docId: resultDocs) {
                    temp = calculateIdf(postingList,totalDoc) * ((BM25_K1+1)*calculateWeightedTf(docId,postingList)) * ((BM25_K3 + 1)*queryData.get(queryTerm));
                    temp /= ((BM25_K1*((1-BM25_B) + BM25_B * (documentLength[docId]*1.0/documentAvg)) + calculateWeightedTf(docId, postingList)) * (BM25_K3 + queryData.get(queryTerm)));
                    scores[docId] += temp;
                }
            }

        } catch (IOException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return scores;
    }

    public double [] calculateLanguangeModelScores(HashMap<String, Integer> queryData, Set<Integer> resultDocs) {
        int totalDoc = totalDocument();
        double[] scores = new double[totalDoc];
        try {
            if (documentLength == null) documentLength = readDocumentLength();
            int totalTerm = getTotalTermAStemmingFromAllDocs();
            TreeMap<Integer, Posting> postingList;
            double ptkmd, ptkmc;
            for(String queryTerm: queryData.keySet()) {
                postingList = fetchPostingList(queryTerm);
                for(int docId: resultDocs) {
                    if (postingList.get(docId) == null) ptkmd = 0;
                    else {
                        ptkmd = postingList.get(docId).getPosition().size()*1.0/documentLength[docId];
                    }
                    ptkmc = 0;
                    for (int docIds: postingList.keySet()) {
                        ptkmc += postingList.get(docIds).getPosition().size();
                    }
                    ptkmc = ptkmc*1.0/totalTerm;
                    if (scores[docId] == 0)
                        scores[docId] = (LAMDA*ptkmd + ((1-LAMDA)*ptkmc));
                    else
                        scores[docId] *= (LAMDA*ptkmd + ((1-LAMDA)*ptkmc));

                }
            }
        } catch(IOException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return scores;
    }

    public double [] calculateCosineScores(HashMap<String, Integer> queryData, Set<Integer> resultDocs) {
        int totalDoc = totalDocument();
        double[] scores = new double[totalDoc];
        try {
            double[] magnitude = this.cosineNormalizationData;
            TreeMap<Integer, Posting> postingList;
            double tfidfQuery;
            for (String queryTerm : queryData.keySet()) {
                postingList = fetchPostingList(queryTerm);
                tfidfQuery = calculateWeightedTfIdfQuery(queryData.get(queryTerm));
                for (int docId : resultDocs) {
//                    System.out.println(calculateDocumentWeightedTfIdf(docId, postingList, totalDoc));
                    scores[docId] += tfidfQuery * calculateDocumentWeightedTfIdf(docId, postingList, totalDoc);
                }
            }

            for (int i = 0; i < scores.length; i++) {
//                System.out.println("xx : " + magnitude[i]);
                scores[i] /= magnitude[i];
            }
        }catch(IOException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return scores;
    }

    public double calculateDocumentWeightedTfIdf(int docId, TreeMap<Integer, Posting> data, int totalDocument) {
        return calculateWeightedTf(docId, data) * calculateIdf(data, totalDocument);
    }

    public int totalDocument() {
        return ApplicationUtil.getInstance().getTotalNumberOfFiles(ApplicationUtil.getInstance().getDocumentData());
    }

    public void writeCosineNormalizationData(double [] data) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(new File(getCosineNormalizationPath(), "data.twise")));
        writer.write(new Gson().toJson(data));
        writer.flush();
        writer.close();

        Logger.getInstance().info("Success writing cosine normalization data!");
    }

    public Document getDocumentDataContent(int i) throws IOException {
        StringBuilder content;
        String temp;
        BufferedReader reader;
        reader = new BufferedReader(new FileReader(new File(getDocumentData(), i+".twise")));
        content = new StringBuilder();
        while((temp = reader.readLine()) != null) {
            content.append(temp);
        }
        return new Gson().fromJson(content.toString(), Document.class);
    }

    private double [] readCosineNormalizationData() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(new File(getCosineNormalizationPath(), "data.twise")));
        StringBuilder content = new StringBuilder();
        String temp;
        while((temp = reader.readLine()) != null) {
            content.append(temp);
        }
        Type type = new TypeToken<double[]>() {
        }.getType();
        return new Gson().fromJson(content.toString(), type);
    }

    private int [] readDocumentLength() throws IOException {
        int total = totalDocument();
        int [] result = new int[total];
        BufferedReader reader;
        StringBuilder content;
        String temp;
        for(int i = 0; i < total; i++) {
            content = new StringBuilder();
            reader = new BufferedReader(new FileReader(new File(ApplicationUtil.getInstance().getDocumentData(), i + ".twise")));
            while ((temp = reader.readLine()) != null) {
                content.append(temp);
            }
            result[i] = new Gson().fromJson(content.toString(), Document.class).getTotalTerm();
        }
        return result;
    }

    private TreeMap<Integer, Posting> fetchPostingList(String term) throws FileNotFoundException, IOException {
        StringBuilder content = new StringBuilder();
        String temp;
        BufferedReader reader = new BufferedReader(new FileReader(new File(ApplicationUtil.getInstance().getDataSortedBlock(), term.charAt(0) + ".twise")));
        while ((temp = reader.readLine()) != null) {
            content.append(temp);
        }
        Type type = new TypeToken<TreeMap<String, TreeMap<Integer, Posting>>>() {
        }.getType();
        TreeMap<String, TreeMap<Integer, Posting>> temp2 = new Gson().fromJson(content.toString(), type);
        return temp2.get(term);
    }

    private double calculateWeightedTf(int docId, TreeMap<Integer, Posting> data) {
        if (data.get(docId) == null) {
            return 0;
        } else {
            return data.get(docId).getPosition().size() == 0 ? 0: 1 + Math.log10(data.get(docId).getPosition().size());
        }
    }

    private double calculateIdf(TreeMap<Integer, Posting> data, int totalDocument) {
//        if (data.size() == 0) System.out.println("ASDF");
        return data.size() == 0? 0 : Math.log10(totalDocument * 1.0 / data.size() * 1.0);
    }

    private double calculateWeightedTfIdfQuery(int count) {
        return count == 0 ? 0 : 1 + Math.log10(count);
    }

    private int calculateDistance(String s1, String s2) {
        int [][] dp = new int[s1.length()+1][s2.length()+1];
        for (int i = 0; i <= s1.length(); i++) {
            dp[i][0] = i;
        }

        for(int i = 0; i <= s2.length(); i++) {
            dp[0][i] = i;
        }

        for (int i = 1; i <= s1.length(); i++) {
            for(int j = 1; j <= s2.length(); j++) {
                if (s1.charAt(i-1) == s2.charAt(j-1)) {
                    dp[i][j] = Math.min(dp[i][j-1], Math.min(dp[i-1][j-1], dp[i-1][j]));
                } else {
                    dp[i][j] = Math.min(dp[i][j-1]+1, Math.min(dp[i-1][j-1]+1, dp[i-1][j]+1));
                }
            }
        }

        return dp[s1.length()][s2.length()];
    }

    public Set<String> getWordSuggestion(String term, TreeMap<String, TreeMap<Integer, Posting>> listMap) {
        String temp1 = null, temp2 = null, temp3 = null;
        int vtemp1 = Integer.MAX_VALUE, vtemp2 = Integer.MAX_VALUE, vtemp3 = Integer.MAX_VALUE;
        int temp;
        for (String termList: listMap.keySet()) {
            temp = calculateDistance(term, termList);
            if (vtemp1 == Integer.MAX_VALUE) { temp1 = termList; vtemp1 = temp; }
            else if (vtemp2 == Integer.MAX_VALUE) { temp2 = termList; vtemp2 = temp; }
            else if (vtemp3 == Integer.MAX_VALUE) { temp3 = termList; vtemp3 = temp; }
            else {
                if (temp < vtemp1) { temp1 = termList; vtemp1 = temp; }
                else if (temp < vtemp2) { temp2 = termList; vtemp2 = temp; }
                else if (temp < vtemp3) { temp3 = termList; vtemp3 = temp; }
            }
        }
        return new HashSet<>(Arrays.asList(temp1, temp2, temp3));
    }
}