/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;

/**
 *
 * @author i16072
 */
public class Logger {
    private AtomicInteger logId;
    private BufferedWriter writer;
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd MMMM yyyy HH:mm:ss");
    private static final String ERROR_LABEL = " : Error - ";
    private static final String DEBUG_LABEL = " : Debug - ";
    private static final String INFO_LABEL = " : Info - ";
    
    private static class LoggerCreator {
        private static final Logger INSTANCE = new Logger();
    }

    public static Logger getInstance() {
        return LoggerCreator.INSTANCE;
    }
    
    private Logger() {
        try {
            System.out.println(System.getProperty("user.dir"));
            this.logId = new AtomicInteger(0);
            this.writer = new BufferedWriter(new FileWriter(new File(System.getProperty("user.dir"), "log.twise"), true));
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public int error(String message) throws IOException {
        if (this.writer != null) {
            return writeSentence(DATE_FORMAT.format(new Date()), ERROR_LABEL, message);
        }
        return -1;
    }
    
    public int info(String message) throws IOException {
        if (this.writer != null) {
            return writeSentence(DATE_FORMAT.format(new Date()), INFO_LABEL, message);
        }
        return -1;
    }
    
    public int debug(String message) throws IOException {
        if (this.writer != null) {
            return writeSentence(DATE_FORMAT.format(new Date()), DEBUG_LABEL, message);
        }
        return -1;
    }
    
    private int writeSentence(String ... sentences) throws IOException {
        this.printIdLabel();
        for (String sentence : sentences) {
            this.writer.write(sentence);
            if (ApplicationUtil.getInstance().isVerbose()) { System.out.print(sentence); }
        }
        this.writer.write(System.lineSeparator());
        System.out.println();
        this.writer.flush();
        return this.logId.addAndGet(1);
    }
    
    private void printIdLabel() throws IOException {
        if (ApplicationUtil.getInstance().isVerbose()) { System.out.print("ID: " + this.logId.get() + " | "); }
        this.writer.write("ID: " + this.logId.get() + " | ");
    }
}
