package util;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import indexer.SPIMI;
import model.*;
import tokenize.Tokenizer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.*;

import tokenize.Lemmatizer;
import tokenize.Lemmatizer.Result;
import tokenize.PorterStemmer;

/**
 * Parser to read the documents
 *
 * @author TwiseAdmin
 */
public class Parser {

    private String [] fileList;

    private static class ParserCreator {
        private static final Parser INSTANCE = new Parser();
    }

    public static Parser getInstance() {
        return ParserCreator.INSTANCE;
    }

    private Parser() {
        ApplicationUtil.getInstance();
        Lemmatizer.getInstance();
    }

    public void updateDocumentFilename() {
        if (ApplicationUtil.getInstance().getDocumentSourcePath() == null) {
            this.fileList = new String[154];
            for(int i = 1; i < 155; i++) {
                this.fileList[i-1] = "Doc";
                if (i < 10) this.fileList[i-1] += "00";
                else if (i < 100) this.fileList[i-1] += "0";
                this.fileList[i-1] += i + ".txt";
            }
        } else {
            this.fileList = ApplicationUtil.getInstance().getAllFilename(ApplicationUtil.getInstance().getAllFiles(ApplicationUtil.getInstance().getDocumentSourcePath()));
        }
    }

    public String getAllIndex() throws IOException {
        File [] files = new File(ApplicationUtil.getInstance().getDataSortedBlock()).listFiles();
        String [] filenames = ApplicationUtil.getInstance().getAllFilename(files);
        BufferedReader reader;
        StringBuilder content;
        String temp;
        JsonObject result = new JsonObject();
        char i = 'a';
        if (filenames != null) {
            Arrays.sort(filenames);
            for (String filename : filenames) {
                if (i > 'z') i = '~';
                if (filename.charAt(0) != i) {
                    while (filename.charAt(0) != i) {
                        result.add(Character.toString(i), JsonNull.INSTANCE);
                        i++;
                    }
                    reader = new BufferedReader(new FileReader(new File(ApplicationUtil.getInstance().getDataSortedBlock(), i + ".twise")));
                    content = new StringBuilder();
                    while ((temp = reader.readLine()) != null) content.append(temp);

                    result.add(Character.toString(i), new Gson().fromJson(content.toString(), JsonElement.class));
                    i++;
                } else {
                    reader = new BufferedReader(new FileReader(new File(ApplicationUtil.getInstance().getDataSortedBlock(), i + ".twise")));
                    content = new StringBuilder();
                    while ((temp = reader.readLine()) != null) content.append(temp);

                    result.add((i == '~' ? "’" : Character.toString(i)), new Gson().fromJson(content.toString(), JsonElement.class));
                    i++;
                }
            }
        }
        return result.toString();
    }

    /**
     * Do the indexing for all the files given
     *
     * @throws IOException
     */
    public void automateIndexing() throws IOException {
        long start = System.currentTimeMillis();
        String[] stopWords = getStopWords();
        List<List<Term>> res = new ArrayList<>();            //list of terms
        for (String filename : this.fileList) {
            BufferedReader reader;
            if (ApplicationUtil.getInstance().getDocumentSourcePath() == null) {
                reader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/lib/" + filename)));
            } else {
                reader = new BufferedReader(new FileReader(new File(ApplicationUtil.getInstance().getDocumentSourcePath(), filename)));
            }
            StringBuilder content = new StringBuilder();
            String input;
            //read the document
            while ((input = reader.readLine()) != null) {
                content.append(input).append("\n");
            }
            //tokenize the document content
            Tokenizer tokenizer = new Tokenizer(content.toString(), stopWords, filename.substring(0, filename.indexOf('.')), filename.substring(filename.indexOf('.') + 1));
            tokenizer = tokenizer.caseFolding().cleanToken().lemmatization().removeStopWords().stemming();
            res.add(tokenizer.finalizeTokenizing());
            Logger.getInstance().info("Finished reading file: " + filename);
            Logger.getInstance().info("ID: " + tokenizer.getDocs().getId());
            Logger.getInstance().info(String.format("Term: %d, Final term: %d\n", tokenizer.getDocs().getTotalTerm(), tokenizer.getDocs().getTotalTermAfterStemming()));
            Logger.getInstance().info("=============================================================================");
        }

        //do SPIMI index construction
        SPIMI spimi = new SPIMI(ApplicationUtil.getInstance().getMaxMemoryIndexing()); //in bytes
        spimi.doSPIMIInvert(res);

        ApplicationUtil.getInstance().printLog();

        spimi.merge();

        long end = System.currentTimeMillis();
        double time = (end - start) * 1.0 / 1000.0;
        double minute = time / 60;
        double seconds = time % 60;
        Logger.getInstance().info("Pre processing finished in " + minute + " minute(s) " + seconds + " second(s)");
    }

    private QueryData parseQuery(String query) throws IOException {
        while (query.contains("  ")) {
            query = query.replaceAll(" {2}", " ");
        }
        String[] temp = query.replaceAll("\\(", "").replaceAll("\\)", "").toLowerCase().split(" ");
        ArrayList<String> termQuery = new ArrayList<>();
        ArrayList<String> operationQuery = new ArrayList<>();
        boolean isOperator = false;
        for (String s : temp) {
            if (isOperator) {
                if (s.equals("or") || s.equals("|")) {
                    operationQuery.add("or");
                    isOperator = false;
                } else if (s.equals("and") || s.equals("&")) {
                    operationQuery.add("and");
                    isOperator = false;
                } else {
                    termQuery.add(s);
                    operationQuery.add("or");
                }
            } else {
                termQuery.add(s);
                isOperator = true;
            }
        }

        StringBuilder sentenceQuery = new StringBuilder();
        for (String a : termQuery) {
            sentenceQuery.append(a).append(" ");
        }

        sentenceQuery = new StringBuilder(sentenceQuery.toString().trim());

//        System.out.println(sentenceQuery.toString());
//        StringBuilder jjj = new StringBuilder();
//        for(String asxd: operationQuery) jjj.append(asxd).append(" ");
//
//        System.out.println(jjj.toString());

        List<Term> termList;

        Result res = Lemmatizer.getInstance().lemmatize(sentenceQuery.toString(), 0);
        termList = res.getLemmas();

        Iterator<Term> it;
        String[] stopWords = ApplicationUtil.getInstance().getStopWords();
        for (String stopword : stopWords) {
            it = termList.iterator();
            while (it.hasNext()) {
                if (it.next().getLemmatizeWord().equals(stopword)) {
                    it.remove();
                }
            }
        }

        for (Term term : termList) {
            term.setLemmatizeWord(PorterStemmer.getInstance().stem(term.getLemmatizeWord()));
        }

        List<TreeMap<String, TreeMap<Integer, Posting>>> listMap = new ArrayList<>();

        for (Term a : termList) {
            StringBuilder content = new StringBuilder();
            String temp3;
            BufferedReader reader = new BufferedReader(new FileReader(new File(ApplicationUtil.getInstance().getDataSortedBlock(), a.getLemmatizeWord().charAt(0) + ".twise")));
            while ((temp3 = reader.readLine()) != null) {
                content.append(temp3);
            }
            Type type = new TypeToken<TreeMap<String, TreeMap<Integer, Posting>>>() {
            }.getType();
            listMap.add(new Gson().fromJson(content.toString(), type));
        }

        return QueryData.getInstance(termList, operationQuery, listMap);
    }

    public String doBooleanQuery(String query) throws IOException {
        long start = System.currentTimeMillis();
        QueryData queryData = parseQuery(query);

        ArrayList<Integer>[] docIds = new ArrayList[queryData.getListMap().size()];

        for (int i = 0; i < queryData.getListMap().size(); i++) {
            docIds[i] = getDocumentIdFromMap(queryData.getTermQuery().get(i).getLemmatizeWord(), queryData.getListMap().get(i));
        }

        for (int i = 0; i < queryData.getOperationQuery().size(); i++) {
            if (queryData.getOperationQuery().get(i).equals("or")) {
                docIds[i + 1].addAll(docIds[i]);
            } else {
                ArrayList<Integer> temp6 = docIds[i + 1];
                docIds[i + 1] = new ArrayList<>();
                for (int x : temp6) {
                    for (int y : docIds[i]) {
                        if (x == y) {
                            docIds[i + 1].add(x);
                        }
                    }
                }
            }
        }
        Set<Integer> x = new TreeSet<>();

        for (int y : docIds[docIds.length - 1]) {
            x.add(y + 1);
        }
        long end = System.currentTimeMillis();
        long total = end - start;
        JsonObject a = new JsonObject();
        a.addProperty("data", new Gson().toJson(x));
        a.addProperty("time", total);
//        System.out.println(a.toString());
        return a.toString();
    }
    
    public String doRankedQuery(String query) throws IOException {
        long start = System.currentTimeMillis();
        QueryData queryData = parseQuery(query);

        HashMap<String, Integer> tempData = new HashMap<>();
        
        for (Term term : queryData.getTermQuery()) {
            tempData.merge(term.getLemmatizeWord(), 1, Integer::sum);
        }

        Set<Integer> x = new TreeSet<>();

        if (tempData.size() == 1) {
            x.addAll(getDocumentIdFromMap(queryData.getTermQuery().get(0).getLemmatizeWord(), queryData.getListMap().get(0)));
        } else {
            ArrayList<Integer>[] docIds = new ArrayList[queryData.getListMap().size()];

            for (int i = 0; i < queryData.getListMap().size(); i++) {
                docIds[i] = getDocumentIdFromMap(queryData.getTermQuery().get(i).getLemmatizeWord(), queryData.getListMap().get(i));
            }

            for(int i = 0;i < docIds.length; i++) {
                System.out.println(new Gson().toJson(docIds[i]));
            }

            for (int i = 0; i < queryData.getOperationQuery().size(); i++) {
                if (queryData.getOperationQuery().get(i).equals("or")) {
                    docIds[i + 1].addAll(docIds[i]);
                } else {
                    ArrayList<Integer> temp6 = docIds[i + 1];
                    docIds[i + 1] = new ArrayList<>();
                    for (int z : temp6) {
                        for (int y : docIds[i]) {
                            if (z == y) {
                                docIds[i + 1].add(z);
                            }
                        }
                    }
                }
            }

            x.addAll(docIds[docIds.length - 1]);
        }

        System.out.println(new Gson().toJson(x));
        Logger.getInstance().info(new Gson().toJson(x));

        JsonObject result = new JsonObject();

        long startAlg = System.currentTimeMillis();
        double [] cosineScore = ApplicationUtil.getInstance().calculateCosineScores(tempData, x);
        long endAlg = System.currentTimeMillis();
        long cosineTime = endAlg-startAlg;
//        System.out.println("A");

        startAlg = System.currentTimeMillis();
        double [] bm25 = ApplicationUtil.getInstance().calculateBM25Scores(tempData, x);
        endAlg = System.currentTimeMillis();
        long bm25Time = endAlg-startAlg;
//        System.out.println("B");
        startAlg = System.currentTimeMillis();
        double [] languageModel = ApplicationUtil.getInstance().calculateLanguangeModelScores(tempData, x);
        endAlg = System.currentTimeMillis();
        long languangeModelTime = endAlg-startAlg;

        List<DocumentScores> results = new ArrayList<>();
        Document temp;
        for(int y: x) {
            temp = ApplicationUtil.getInstance().getDocumentDataContent(y);
//            if (cosineScore[temp.getId()] == 0) continue;
            results.add(new DocumentScores(
                    temp.getId() + 1,
                    cosineScore[temp.getId()],
                    temp.getFilename(),
                    temp.getExtension(),
                    temp.getContent(),
                    temp.getTermList(),
                    queryData.getTermQuery(),
                    temp.getTotalTerm(),
                    temp.getTotalWord(),
                    temp.getTotalTermAfterLemmatization(),
                    temp.getTotalTermAfterRemovingStopWords()
            ));
        }

        Collections.sort(results);

        JsonObject cosineResult = new JsonObject();
        if (ApplicationUtil.getInstance().getTopRank() != 0)
            cosineResult.addProperty("data", new GsonBuilder().disableHtmlEscaping().create().toJson(results.subList(0,ApplicationUtil.getInstance().getTopRank())));
        else
            cosineResult.addProperty("data", new GsonBuilder().disableHtmlEscaping().create().toJson(results));
        cosineResult.addProperty("time", cosineTime);
        result.add("cosine_similarity", cosineResult);

        results = new ArrayList<>();
        for(int y: x) {
            temp = ApplicationUtil.getInstance().getDocumentDataContent(y);
//            if (cosineScore[temp.getId()] == 0) continue;
            results.add(new DocumentScores(
                    temp.getId() + 1,
                    bm25[temp.getId()],
                    temp.getFilename(),
                    temp.getExtension(),
                    temp.getContent(),
                    temp.getTermList(),
                    queryData.getTermQuery(),
                    temp.getTotalTerm(),
                    temp.getTotalWord(),
                    temp.getTotalTermAfterLemmatization(),
                    temp.getTotalTermAfterRemovingStopWords()
            ));
        }

        Collections.sort(results);

        JsonObject bm25Result = new JsonObject();
        if (ApplicationUtil.getInstance().getTopRank() != 0)
            bm25Result.addProperty("data", new GsonBuilder().disableHtmlEscaping().create().toJson(results.subList(0,ApplicationUtil.getInstance().getTopRank())));
        else
            bm25Result.addProperty("data", new GsonBuilder().disableHtmlEscaping().create().toJson(results));
        bm25Result.addProperty("time", bm25Time);
        result.add("bm25", bm25Result);

        results = new ArrayList<>();
        for(int y: x) {
            temp = ApplicationUtil.getInstance().getDocumentDataContent(y);
//            if (cosineScore[temp.getId()] == 0) continue;
            results.add(new DocumentScores(
                    temp.getId() + 1,
                    languageModel[temp.getId()],
                    temp.getFilename(),
                    temp.getExtension(),
                    temp.getContent(),
                    temp.getTermList(),
                    queryData.getTermQuery(),
                    temp.getTotalTerm(),
                    temp.getTotalWord(),
                    temp.getTotalTermAfterLemmatization(),
                    temp.getTotalTermAfterRemovingStopWords()
            ));
        }

        Collections.sort(results);

        JsonObject languangeModelResult = new JsonObject();
        if (ApplicationUtil.getInstance().getTopRank() != 0)
            languangeModelResult.addProperty("data", new GsonBuilder().disableHtmlEscaping().create().toJson(results.subList(0,ApplicationUtil.getInstance().getTopRank())));
        else
            languangeModelResult.addProperty("data", new GsonBuilder().disableHtmlEscaping().create().toJson(results));
        languangeModelResult.addProperty("time", languangeModelTime);
        result.add("language_model", languangeModelResult);

        if (results.size() <= ApplicationUtil.getInstance().getTopRank()/2) {
            Set<String> wordSuggestion = new HashSet<>();
//            for(int i = 0; i < queryData.getTermQuery().size(); i++) {
//                TreeMap<String, Integer> suggestionScores = new TreeMap<>(Collections.reverseOrder());
//                for(String xas: queryData.getListMap().get(i).keySet()) {
//                    suggestionScores.put(xas, new FuzzyScore(Locale.ENGLISH).fuzzyScore(queryData.getTermQuery().get(i).getLemmatizeWord(), xas));
//                }
////                if ()
//            }
            for (int i = 0; i < queryData.getTermQuery().size(); i++) {
                wordSuggestion.addAll(ApplicationUtil.getInstance().getWordSuggestion(queryData.getTermQuery().get(i).getLemmatizeWord(), queryData.getListMap().get(i)));
            }
            result.addProperty("suggestion", new GsonBuilder().disableHtmlEscaping().create().toJson(wordSuggestion));

//            System.out.println(new GsonBuilder().disableHtmlEscaping().create().toJson(wordSuggestion));
        }

        long end = System.currentTimeMillis();
        long total = end - start;
        result.addProperty("time", total);

        return result.toString();
    }

    private ArrayList<Integer> getDocumentIdFromMap(String term, TreeMap<String, TreeMap<Integer, Posting>> data) {
        ArrayList<Integer> result = new ArrayList<>();
        for (String a : data.keySet()) {
            if (a.equalsIgnoreCase(term)) {
                TreeMap<Integer, Posting> temp = data.get(a);
                result.addAll(temp.keySet());
            }
        }

        return result;
    }

    private String[] getStopWords() throws IOException {
        return ApplicationUtil.getInstance().getStopWords();
    }
}
