package indexer;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import jdk.jshell.spi.ExecutionControl;
import model.Posting;
import model.Term;
import util.ApplicationUtil;
import util.Logger;

import java.io.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/**
 * Class for constructing the indexes
 *
 * @author TwiseAdmin
 */
public class SPIMI {

    private final long maxMemory;     //memory limit for this indexing

    public SPIMI(long maxMemory) {
        this.maxMemory = maxMemory;
    }

    /**
     * Construct the index by the given term and document id. If the memory used
     * over the limit, it will immidiately stored in disk.
     * See the @file Report.docx file for more detail about the data structure
     *
     * @param termLists the term list that will be indexed
     * @throws IOException if the temp spimi block not found
     */
    public void doSPIMIInvert(List<List<Term>> termLists) throws IOException {
        long initialMemory = Runtime.getRuntime().freeMemory();
        long usedMemory = 0;

        TreeMap<String, TreeMap<Integer, Posting>> dictionary = new TreeMap<>();
        TreeMap<Integer, Posting> postingLists;
        Posting posting;

        for (List<Term> termList : termLists) {
            for (Term term : termList) {
                //Write to disk if memory used bigger than max memory
                if (usedMemory >= this.maxMemory) {
                    for (String temp1 : dictionary.keySet()) {
                        int temp3 = -1;
                        int i = 4;
                        TreeMap<Integer, Posting> temp4 = dictionary.get(temp1);
                        for (Integer temp2 : temp4.keySet()) {
                            if (i == 4) {
                                temp3 = temp2;
                            } else if (i == 0) {
                                temp4.get(temp2).setPointer(temp3);
                                temp3 = temp2;
                                i = 4;
                            }
                            i--;
                        }
                    }
                    ApplicationUtil.getInstance().writeInvertedIndexBlock(dictionary);
                    dictionary = new TreeMap<>();
                    System.gc();
                    initialMemory = Runtime.getRuntime().freeMemory();
                }

                long currentMemory = Runtime.getRuntime().freeMemory();
                usedMemory = Math.abs(initialMemory - currentMemory);

                if (dictionary.get(term.getLemmatizeWord()) == null) {
                    postingLists = new TreeMap<>();
                    dictionary.put(term.getLemmatizeWord(), postingLists);
                } else {
                    postingLists = dictionary.get(term.getLemmatizeWord());
                }

                if (postingLists.get(term.getDocId()) == null) {
                    posting = new Posting(new ArrayList<>());
                    postingLists.put(term.getDocId(), posting);
                } else {
                    posting = postingLists.get(term.getDocId());
                }

                posting.getPosition().add(term.getPosition());
            }
        }

        for (String temp1 : dictionary.keySet()) {
            int temp3 = -1;
            int i = 3;
            TreeMap<Integer, Posting> temp4 = dictionary.get(temp1);
            for (Integer temp2 : temp4.keySet()) {
                if (i == 3) {
                    temp3 = temp2;
                } else if (i == 0) {
                    temp4.get(temp3).setPointer(temp2);
                    temp3 = temp2;
                    i = 3;
                }
                i--;
            }
        }
        ApplicationUtil.getInstance().writeInvertedIndexBlock(dictionary);
        dictionary = null;
        System.gc();
    }

    /**
     * Using map reduce concept to merge all the block in temp directory.
     * This map reduce use only the first char for each term for mapping.
     */
    public void merge() {
        try {
            Logger.getInstance().info("Performing map reduce on inverted index");
            long start = System.currentTimeMillis();
            File dest;
            File[] sources = ApplicationUtil.getInstance().getAllFiles(ApplicationUtil.getInstance().getDataTempBlock());
            StringBuilder content;
            String temp;
            BufferedReader reader;
            TreeMap<String, TreeMap<Integer, Posting>> dictionary;
            double [] cosineNormalizationData = new double[ApplicationUtil.getInstance().totalDocument()];
            double [] cosineTemp;
            for (File source : sources) {
                reader = new BufferedReader(new FileReader(source));
                content = new StringBuilder();
                while ((temp = reader.readLine()) != null) {
                    content.append(temp).append("\n");
                }
                Type type = new TypeToken<TreeMap<String, TreeMap<Integer, Posting>>>() {
                }.getType();
                dictionary = new Gson().fromJson(content.toString(), type);
                char i = 'a';
                dest = new File(ApplicationUtil.getInstance().getDataSortedBlock(), i + ".twise");
                BufferedWriter writer = new BufferedWriter(new FileWriter(dest, true));
                TreeMap<String, TreeMap<Integer, Posting>> finalDictionary = new TreeMap<>();
                for (String key : dictionary.keySet()) {
                    if (key.charAt(0) == '’') {
                        writer.write(new Gson().toJson(finalDictionary));
                        writer.flush();
                        writer.close();
                        dest = new File(ApplicationUtil.getInstance().getDataSortedBlock(), "~.twise");
                        writer = new BufferedWriter(new FileWriter(dest, true));
                        finalDictionary = new TreeMap<>();
                    } else if (key.charAt(0) != i) {
                        writer.write(new Gson().toJson(finalDictionary));
                        while (i != key.charAt(0)) i++;
                        writer.flush();
                        writer.close();
                        dest = new File(ApplicationUtil.getInstance().getDataSortedBlock(), i + ".twise");
                        writer = new BufferedWriter(new FileWriter(dest, true));
                        finalDictionary = new TreeMap<>();
                    }
                    finalDictionary.put(key, dictionary.get(key));
                }
                writer.flush();
                writer.close();
                cosineTemp = calculateTfIdfForCosineNormalization(dictionary);
                for(int j = 0; j < cosineNormalizationData.length; j++) {
                    cosineNormalizationData[j] += cosineTemp[j];
                }
            }

            long finish = System.currentTimeMillis();
            Logger.getInstance().info("Finish writing merged block with map reduce in: " + (finish-start) + "milisecond(s)");

            for(int i = 0; i < cosineNormalizationData.length; i++) {
                cosineNormalizationData[i] = Math.sqrt(cosineNormalizationData[i]);
            }

            createCosineNormalizationData(cosineNormalizationData);
        } catch(IOException ignored){}
    }

    private String getCompressedDictionary(String dictionary) throws ExecutionControl.NotImplementedException {
        //TODO: create compression method
        throw new ExecutionControl.NotImplementedException("This method not supported yet.");
    }

    private String getCompressedPosting(String posting) throws ExecutionControl.NotImplementedException {
        //TODO: create compression method
        throw new ExecutionControl.NotImplementedException("This method not supported yet.");
    }

    private void createCosineNormalizationData(double [] data) throws IOException {
        ApplicationUtil.getInstance().writeCosineNormalizationData(data);
    }

    private double [] calculateTfIdfForCosineNormalization(TreeMap<String, TreeMap<Integer, Posting>> dictionary) {
        int totalDoc = ApplicationUtil.getInstance().totalDocument();
        double [] result = new double[totalDoc];
        TreeMap<Integer, Posting> postingLists;
        for(String term: dictionary.keySet()) {
            postingLists = dictionary.get(term);
            for(int docId: postingLists.keySet()) {
                result[docId] += ApplicationUtil.getInstance().calculateDocumentWeightedTfIdf(docId, postingLists, totalDoc);
            }
        }

        return result;
    }
}
