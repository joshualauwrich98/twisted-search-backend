package tokenize;

/**
 * All stemmer class must implement this interface.
 * @author TwiseAdmin
 */
public interface Stemming {
    /**
     * Stem the given string to its basic form.
     * @param word the word to be stemed
     * @return the result of stemming
     */
    String stem(String word);
}
