package tokenize;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import model.Term;
import util.ApplicationUtil;

import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;

/**
 * This class represents a factory class to perform lemmatization for any given input string.
 * @author TwiseAdmin
 */
public class Lemmatizer implements Lemmatization {
    public final static String IP_LEMMA = "IP";
    public final static String NUMBER_LEMMA = "NUMBER";
    public final static String DATE_LEMMA = "DATE";
    public final static String TIME_LEMMA = "TIME";
    public final static String PERSON_LEMMA = "PERSON";
    public final static String LOCATION_LEMMA = "LOCATION";
    public final static String OBJECT_LEMMA = "OBJECT";
    
    private StanfordCoreNLP pipeline;

    private static Pattern IP_PATTERN = Pattern.compile(
            "^([01]?\\\\d\\\\d?|2[0-4]\\\\d|25[0-5])\\\\.([01]?\\\\d\\\\d?|2[0-4]\\\\d|25[0-5])\\\\." +
                    "([01]?\\\\d\\\\d?|2[0-4]\\\\d|25[0-5])\\\\.([01]?\\\\d\\\\d?|2[0-4]\\\\d|25[0-5])$");

    private static Pattern DATE_PATTERN = Pattern.compile(
            "^((2000|2400|2800|(19|2[0-9](0[48]|[2468][048]|[13579][26])))-02-29)$"
                    + "|^(((19|2[0-9])[0-9]{2})-02-(0[1-9]|1[0-9]|2[0-8]))$"
                    + "|^(((19|2[0-9])[0-9]{2})-(0[13578]|10|12)-(0[1-9]|[12][0-9]|3[01]))$"
                    + "|^(((19|2[0-9])[0-9]{2})-(0[469]|11)-(0[1-9]|[12][0-9]|30))$");
    
    private static class LemmatizerCreator {
        private static final Lemmatizer INSTANCE = new Lemmatizer();
    }

    public static Lemmatizer getInstance() {
        return LemmatizerCreator.INSTANCE;
    }
    
    private Lemmatizer() {
        Properties props = new Properties();
        if (ApplicationUtil.getInstance().isNer()) props.put("annotators", "tokenize, ssplit, pos, lemma, ner");
        else props.put("annotators", "tokenize, ssplit, pos, lemma");
        this.pipeline = new StanfordCoreNLP(props);
    }

    /**
     * Change the configuration after changing the setting. Turning on the NER capabilities.
     */
    public void makeNERAvailable() {
        Properties props = new Properties();
        props.put("annotators", "tokenize, ssplit, pos, lemma, ner");
        this.pipeline = new StanfordCoreNLP(props);
    }
    /**
     * Change the configuration after changing the setting. Turning off the NER capabilities.
     */
    public void makeNERUnavailable() {
        Properties props = new Properties();
        props.put("annotators", "tokenize, ssplit, pos, lemma");
        this.pipeline = new StanfordCoreNLP(props);
    }
    
    @Override
    public Result lemmatize(String input, int docId) {
        while (input.contains("  ")) input = input.replaceAll(" {2}", " ");

        String [] splittedStr = input.split(" ");
        Annotation document;
        List<CoreMap> sentences;
        List<Term> lemmas = new LinkedList<>();
        int totalNumber = 0, totalIp = 0, totalDate = 0, totalObject = 0,
                totalTime = 0, totalPerson = 0, totalLocation = 0;
        String temp;
        int i = 0;

        for(String word: splittedStr) {
            document = new Annotation(word);
            this.pipeline.annotate(document);
            sentences = document.get(SentencesAnnotation.class);
            for (CoreMap sentence: sentences) {
                for(CoreLabel token: sentence.get(TokensAnnotation.class)) {
                    temp = token.get(LemmaAnnotation.class);
                    temp = temp.trim().toLowerCase();
                    if (temp.equals("") || temp.equals(" ") || temp.equals(".") || temp.equals(",")
                            || temp.equals("?") || temp.equals("!") || temp.equals("'") || temp.equals("-")
                            || temp.equals("-lsb-") || temp.equals("-rsb-")) continue;
                    if (ApplicationUtil.getInstance().isNer()) {
                        if (token.get(CoreAnnotations.NamedEntityTagAnnotation.class).equals("TIME")) {
                            totalTime++;
                            lemmas.add(new Term(word, temp, TIME_LEMMA, i++, docId));
                        } else if (token.get(CoreAnnotations.NamedEntityTagAnnotation.class).equals("DATE")) {
                            lemmas.add(new Term(word, temp, DATE_LEMMA, i++, docId));
                            totalDate++;
                        } else if (token.get(CoreAnnotations.NamedEntityTagAnnotation.class).equals("NUMBER")) {
                            if (isIpAdress(temp)) {
                                lemmas.add(new Term(word, temp, IP_LEMMA, i++, docId));
                                totalIp++;
                            }
                            else {
                                if (token.get(CoreAnnotations.NormalizedNamedEntityTagAnnotation.class) != null ||
                                        !token.get(CoreAnnotations.NormalizedNamedEntityTagAnnotation.class).equals("")) {
                                    lemmas.add(new Term(word, token.get(CoreAnnotations.NormalizedNamedEntityTagAnnotation.class), NUMBER_LEMMA, i++, docId));
                                    totalNumber++;
                                }
                            }
                        } else  if (token.get(CoreAnnotations.NamedEntityTagAnnotation.class).equals("PERSON")) {
                            totalPerson++;
                            lemmas.add(new Term(word, temp, PERSON_LEMMA, i++, docId));
                        } else if (token.get(CoreAnnotations.NamedEntityTagAnnotation.class).equals("LOCATION")) {
                            totalLocation++;
                            lemmas.add(new Term(word, temp, LOCATION_LEMMA, i++, docId));
                        } else {
                            lemmas.add(new Term(word, temp, OBJECT_LEMMA, i++, docId));
                            totalObject++;
                        }
                    } else {
                        lemmas.add(new Term(word, temp, OBJECT_LEMMA, i++, docId));
                    }
                }
            }
        }
        return new Result(lemmas, lemmas.size(), totalNumber, totalIp, totalDate, totalTime, totalPerson, totalLocation, totalObject);
    }
    
    private boolean isIpAdress(String word) {
        return IP_PATTERN.matcher(word).matches();
    }

    /**
     * This class is a container for lemmatization process.
     */
    public static class Result {
        private final List<Term> lemmas;
        private final int totalLemmas, totalNumber, totalIp, totalDate,
                totalTime, totalPerson, totalLocation, totalObject;

        public Result(List<Term> lemmas, int totalLemmas, int totalNumber, int totalIp, int totalDate, int totalTime, int totalPerson, int totalLocation, int totalObject) {
            this.lemmas = lemmas;
            this.totalLemmas = totalLemmas;
            this.totalNumber = totalNumber;
            this.totalIp = totalIp;
            this.totalDate = totalDate;
            this.totalTime = totalTime;
            this.totalPerson = totalPerson;
            this.totalLocation = totalLocation;
            this.totalObject = totalObject;
        }

        public List<Term> getLemmas() {
            return lemmas;
        }

        public int getTotalLemmas() {
            return totalLemmas;
        }

        public int getTotalNumber() {
            return totalNumber;
        }

        public int getTotalIp() {
            return totalIp;
        }

        public int getTotalDate() {
            return totalDate;
        }

        public int getTotalTime() {
            return totalTime;
        }

        public int getTotalPerson() {
            return totalPerson;
        }

        public int getTotalLocation() {
            return totalLocation;
        }

        public int getTotalObject() {
            return totalObject;
        }
    }
}
