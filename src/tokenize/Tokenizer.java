package tokenize;

import model.Document;
import model.Term;
import tokenize.Lemmatizer.Result;
import util.ApplicationUtil;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/**
 * This class represent a tokenizer that will be parsing and doing preprocessing tasks such as: normalization, case folding,
 * lemmatization, stemming, and cleaning.
 * @author TwiseAdmin
 */
public class Tokenizer {

    private String fileContent;
    private List<Term> tokens;
    private String[] stopWords;
    private Document docs;

    public Tokenizer(String fileContent, String[] stopWords, String filename, String fileExtension) {
        this.fileContent = fileContent;
        this.stopWords = stopWords;
        this.docs = new Document(ApplicationUtil.getInstance().isNer(), ApplicationUtil.getInstance().getTotalNumberOfFiles(ApplicationUtil.getInstance().getDocumentData()),
                filename, fileExtension, ApplicationUtil.getInstance().countTotalTermFromString(fileContent),
                ApplicationUtil.getInstance().countTotalWordsFromString(fileContent),
                fileContent);
    }

    public Document getDocs() {
        return docs;
    }

    public Tokenizer caseFolding() {
        this.fileContent = this.fileContent.toLowerCase();
        return this;
    }

    /**
     * Clean the content from symbols and other unnecessary characters.
     * @return the instance of this class.
     */
    public Tokenizer cleanToken() {
        this.fileContent = this.fileContent.replaceAll("[;:#*&|]", " ");
        this.fileContent = this.fileContent.replaceAll("\\,*", "");
        this.fileContent = this.fileContent.replaceAll("\\.*", "");
        this.fileContent = this.fileContent.replaceAll("\n|\r", " ");
        this.fileContent = this.fileContent.replaceAll("\"", "");
        this.fileContent = this.fileContent.replaceAll("&lt;", "");
        this.fileContent = this.fileContent.replaceAll("&gt;", "");
        this.fileContent = this.fileContent.replaceAll("\\+", "");
        this.fileContent = this.fileContent.replaceAll("\\(|\\)", "");
        this.fileContent = this.fileContent.replaceAll("\\*", "");
        this.fileContent = this.fileContent.replaceAll("&amp", " ");
        this.docs.setTotalTermAfterCleaning(ApplicationUtil.getInstance().countTotalTermFromString(this.fileContent));

        return this;
    }

    /**
     * Lemmatize the content using Lemmatizer object.
     * @return the instance of this class.
     */
    public Tokenizer lemmatization() {
        Result res = Lemmatizer.getInstance().lemmatize(this.fileContent, this.docs.getId());
        this.docs.setTotalDate(res.getTotalDate());
        this.docs.setTotalIp(res.getTotalIp());
        this.docs.setTotalLocation(res.getTotalLocation());
        this.docs.setTotalNumber(res.getTotalNumber());
        this.docs.setTotalObject(res.getTotalObject());
        this.docs.setTotalPerson(res.getTotalPerson());
        this.docs.setTotalTime(res.getTotalTime());
        this.docs.setTotalTermAfterLemmatization(res.getTotalLemmas());
        this.docs.setTermList(res.getLemmas());

        this.tokens = res.getLemmas();

        this.docs.setTotalTermAfterLemmatization(res.getTotalLemmas());

        return this;
    }

    /**
     * Clean the content from stop words.
     * @return the instance of this class.
     */
    public Tokenizer removeStopWords() {
        Iterator<Term> it;
        for (String stopword : this.stopWords) {
            it = tokens.iterator();
            while(it.hasNext()) {
                if (it.next().getLemmatizeWord().equals(stopword)) {
                    it.remove();
                }
            }
        }

        this.docs.setTotalTermAfterRemovingStopWords(tokens.size());

        return this;
    }

    /**
     * Stem the content using Stemmer object.
     * @return the instance of this class.
     */
    public Tokenizer stemming() {
        for(int i = 0; i < this.tokens.size(); i++) {
            this.tokens.get(i).setLemmatizeWord(PorterStemmer.getInstance().stem(this.tokens.get(i).getLemmatizeWord()));
        }

        this.docs.setTotalTermAfterStemming(this.tokens.size());

        return this;
    }

    /**
     * Perform a finalization of pre processing. This will write all the document data into disk.
     * @return list of term after pre processing.
     * @throws IOException if file not found or permission denied to access the file.
     */
    public List<Term> finalizeTokenizing() throws IOException {
        ApplicationUtil.getInstance().writeDocumentSummary(this.docs);
        return this.tokens;
    }

    @Override
    public String toString() {
        return this.fileContent; //To change body of generated methods, choose Tools | Templates.
    }
}
