package tokenize;

import tokenize.Lemmatizer.Result;

/**
 * All lemmatizer class must implement this interface
 * @author TwiseAdmin
 */
public interface Lemmatization {
    /**
     * Lemmatize the given string
     * @param sentence the input string
     * @param docId the id of the document for this lemmatization
     * @return Result object to represent the final result of the lemmatization
     */
    Result lemmatize(String sentence, int docId);
}
