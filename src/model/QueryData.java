package model;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/**
 * This class represent a data carried by query.
 * @author TwiseAdmin
 */
public class QueryData {
    private ArrayList<String> operationQuery;
    private List<Term> termQuery;
    private List<TreeMap<String, TreeMap<Integer, Posting>>> listMap;
    private static QueryData instance;

    public static QueryData getInstance(List<Term> termQuery, ArrayList<String> operationQuery, List<TreeMap<String, TreeMap<Integer, Posting>>> listMap) {
        if (instance == null) instance = new QueryData();
        instance.termQuery = termQuery;
        instance.operationQuery = operationQuery;
        instance.listMap = listMap;
        return instance;
    }

    public List<Term> getTermQuery() {
        return termQuery;
    }

    public ArrayList<String> getOperationQuery() {
        return operationQuery;
    }

    public List<TreeMap<String, TreeMap<Integer, Posting>>> getListMap() {
        return listMap;
    }

    private QueryData () {
    }
}
