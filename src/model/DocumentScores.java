package model;

import java.util.List;

/**
 * This class represents a document with a score after performing query.
 * @author TwiseAdmin
 */
public class DocumentScores extends Document implements Comparable<DocumentScores> {
    private double score;
    private List<Term> queryTermList;
    private String preview;

    public DocumentScores(int id, double score, String filename, String extension, String content, List<Term> termList,
                          List<Term> queryTermList, int totalTerm, int totalWord, int totalTermAfterLemmatization,
                          int totalTermAfterRemovingStopwords) {
        super(false, id, filename, extension, totalTerm, totalWord, content);
        this.score = score;
        this.termList = termList;
        this.queryTermList = queryTermList;
        this.preview = getPreviewContent();
        this.content = getFullContentWithStyling();
        this.setTotalTermAfterLemmatization(totalTermAfterLemmatization);
        this.setTotalTermAfterRemovingStopWords(totalTermAfterRemovingStopwords);
    }

    public DocumentScores() {
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<Term> getTermList() {
        return termList;
    }

    public void setTermList(List<Term> termList) {
        this.termList = termList;
    }
    
    private String getPreviewContent() {
        String result = this.content;
        boolean isFound = false;
        for (Term queryTerm: this.queryTermList) {
            for (Term term: this.termList) {
                if (term.getLemmatizeWord().equalsIgnoreCase(queryTerm.getLemmatizeWord())) {
                    int index = this.content.indexOf(term.getRealWord());
                    result = ".... " + result.replaceAll(term.getRealWord(), addSpecialClassForStyling(term.getRealWord()));
                    if (index - 75 < 0) {
                        if (index + 75 <= result.length() - 1) {
                            result = ".... " + result.substring(0, (75-index)+150) + " ....";
                        } else {
                            result = ".... " + result + " ....";
                        }
                    } else {
                        if (index + 75 > result.length()-1) {
                            result = ".... " + result.substring(index-150) + " ....";
                        } else {
                            result = ".... " + result.substring(index-75, index+75) + " ....";
                        }
                    }
                    isFound = true;
                }
                if (isFound) break;
            }
            if (isFound) break;
        }
        
        return result;
    }

    /**
     * Get full content with styling for the query term.
     * @return the content with full styling.
     */
    public String getFullContentWithStyling() {
        String result = this.content;
        for (Term queryTerm: this.queryTermList) {
            for (Term term: this.termList) {
                if (term.getLemmatizeWord().equals(queryTerm.getLemmatizeWord())) {
                    result = result.replaceAll(term.getRealWord(), addSpecialClassForStyling(term.getRealWord()));
                }
            }
        }

        return result;
    }

    private String addSpecialClassForStyling(String word) {
        return "<span class=\"query-term\">" + word + "</span>";
    }

    @Override
    public int compareTo(DocumentScores documentScores) {
        return Double.compare(documentScores.getScore(), this.score);
    }
}
