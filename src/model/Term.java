package model;

/**
 * This class represents a term.
 * @author TwiseAdmin
 */
public class Term {
    protected String realWord, lemmatizeWord;
    private String category;
    private int position, docId;

    public String getRealWord() {
        return realWord;
    }

    public void setRealWord(String realWord) {
        this.realWord = realWord;
    }

    public String getLemmatizeWord() {
        return lemmatizeWord;
    }

    public void setLemmatizeWord(String lemmatizeWord) {
        this.lemmatizeWord = lemmatizeWord;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getDocId() {
        return docId;
    }

    public void setDocId(int docId) {
        this.docId = docId;
    }

    public Term(String realWord, String lemmatizeWord, String category, int position, int docId) {
        this.realWord = realWord;
        this.lemmatizeWord = lemmatizeWord;
        this.category = category;
        this.position = position;
        this.docId = docId;
    }

    public Term() {
    }
}
