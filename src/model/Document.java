package model;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a Document
 * @author TwiseAdmin
 */
public class Document {
    protected int id;
    private int totalTerm, totalTermAfterCleaning, totalTermAfterLemmatization,
            totalTermAfterStemming, totalTermAfterRemovingStopWords, totalWord;
    private int totalNumber, totalIp, totalDate,
                totalTime, totalPerson, totalLocation, totalObject;
    protected String filename, extension, content;
    protected List<Term> termList;
    private boolean isNER;

    public Document() {
    }

    public Document(boolean isNER, int id, String filename, String extension, int totalTerm, int totalWord, String content) {
        this.id = id;
        this.filename = filename;
        this.extension = extension;
        this.totalTerm = totalTerm;
        this.totalWord = totalWord;
        this.content = content;
        this.isNER = isNER;
        this.termList = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTotalTerm() {
        return totalTerm;
    }

    public void setTotalTerm(int totalTerm) {
        this.totalTerm = totalTerm;
    }

    public int getTotalTermAfterCleaning() {
        return totalTermAfterCleaning;
    }

    public void setTotalTermAfterCleaning(int totalTermAfterCleaning) {
        this.totalTermAfterCleaning = totalTermAfterCleaning;
    }
    
    public int getTotalTermAfterStemming() {
        return totalTermAfterStemming;
    }

    public void setTotalTermAfterStemming(int totalTermAfterStemming) {
        this.totalTermAfterStemming = totalTermAfterStemming;
    }

    public int getTotalTermAfterRemovingStopWords() {
        return totalTermAfterRemovingStopWords;
    }

    public void setTotalTermAfterRemovingStopWords(int totalTermAfterRemovingStopWords) {
        this.totalTermAfterRemovingStopWords = totalTermAfterRemovingStopWords;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getTotalTermAfterLemmatization() {
        return totalTermAfterLemmatization;
    }

    public void setTotalTermAfterLemmatization(int totalTermAfterLemmatization) {
        this.totalTermAfterLemmatization = totalTermAfterLemmatization;
    }

    public int getTotalNumber() {
        return totalNumber;
    }

    public void setTotalNumber(int totalNumber) {
        this.totalNumber = totalNumber;
    }

    public int getTotalIp() {
        return totalIp;
    }

    public void setTotalIp(int totalIp) {
        this.totalIp = totalIp;
    }

    public int getTotalDate() {
        return totalDate;
    }

    public void setTotalDate(int totalDate) {
        this.totalDate = totalDate;
    }

    public int getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(int totalTime) {
        this.totalTime = totalTime;
    }

    public int getTotalPerson() {
        return totalPerson;
    }

    public void setTotalPerson(int totalPerson) {
        this.totalPerson = totalPerson;
    }

    public int getTotalLocation() {
        return totalLocation;
    }

    public void setTotalLocation(int totalLocation) {
        this.totalLocation = totalLocation;
    }

    public int getTotalObject() {
        return totalObject;
    }

    public void setTotalObject(int totalObject) {
        this.totalObject = totalObject;
    }

    public int getTotalWord() {
        return totalWord;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this, Document.class);
    }

    public List<Term> getTermList() {
        return termList;
    }

    public boolean isNER() {
        return isNER;
    }

    public void setTermList(List<Term> termList) {
        this.termList = termList;
    }
}
