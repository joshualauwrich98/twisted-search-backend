package model;

import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;

/**
 *  This class represent a posting.
 * @author TwiseAdmin
 */
public class Posting {
    private List<Integer> position;
    private int pointer;

    public Posting() {
        this.position = new ArrayList<>();
        this.pointer = -1;
    }
    
    public Posting(List<Integer> position) {
        this.position = position;
        this.pointer = -1;
    }

    public List<Integer> getPosition() {
        return position;
    }

    public void setPosition(List<Integer> position) {
        this.position = position;
    }

    public int getPointer() {
        return pointer;
    }

    public void setPointer(int pointer) {
        this.pointer = pointer;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this, Posting.class);
    }
}